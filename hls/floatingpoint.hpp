#include <stdio.h>
#include <string.h>
#include <ap_int.h>
#include <math.h>

template<int E, int M> class em_float;

#ifndef __EM_FLOAT__
#define __EM_FLOAT__

#ifndef SE
#define SE 2
#define SM 6
#endif

template<int E, int M>
class em_float{
	public:
		em_float(){sig = 0; exp = 0; man = 0;};
		em_float(ap_uint<1> s, ap_uint<E> e, ap_uint<M> m)
		{sig = s; exp = e; man = m;};

		float toFloat();

		ap_uint<1> getSign();
		ap_uint<E> getExponent();
		ap_uint<M> getMantise();

		ap_uint<M+1> getRealMantise();

		bool isZero(){return (exp == 0) && (man == 0);}
		bool isWrong(){return (exp == 0) && (man != 0);}
		bool isNan(){return exp == ((1 << E)-1) && (man != 0);}
		bool isInf(){return exp == ((1 << E)-1) && (man == 0);}

		em_float<E, M> mult(em_float<E, M> b);
		em_float<E, M> add(em_float<E, M> b);

		ap_uint<E+M+1> getBinary();

		ap_uint<E> getBias(){return (1 << (E-1)) - 1;};


	private:
		ap_uint<1> sig;
		ap_uint<E> exp;
		ap_uint<M> man;
};

//WARNING give a false value but never used
template<int E, int M>
inline float em_float<E, M>::toFloat() {
	float res = 1;
	int i;
	for(i = 0; i < M; i++){
		res += ((man >> i) & 1)*pow(2,i-M);
	}
	return res*pow(2, exp);
}
#include "floatingpoint.cpp"
#endif
