#include "floatingpoint.hpp"
#include <stdio.h>
#include <string.h>
#include <ap_int.h>

/*
 * Test function for DMA access.
 */
void DMA_loop(volatile int* data_in,volatile int* data_out){
	#pragma HLS INTERFACE s_axilite port=return
	#pragma HLS INTERFACE m_axi depth=3072 port=data_in offset=slave bundle=data_in
	#pragma HLS INTERFACE m_axi depth=3072 port=data_out offset=slave bundle=data_out

	*data_out++ = *data_in++;
}

/*
 * Matrix vector multiplication A.B = C
 * A : (M by 16) (with 4 float in 32 bits)
 * B : (16)		 (with 4 float in 32 bits)
 * C : (16)		 (with 1 float in 32 bits)
 */
void vector_matrix_mult(volatile ap_uint<32>* data_in, volatile ap_uint<32>*  data_out){
	#pragma AP interface axis port=data_in
	#pragma AP interface axis port=data_out
	#pragma AP interface ap_ctrl_none port=return

	 /* Second dim of the matrix */
	ap_uint<32> m = *data_in++;

	 /* used to store input data */
	ap_uint<32> d;

	 /* save the vector in memory */
	em_float<3,4> vec[16];
	for(int i = 0; i < 4; i++){
		d = *data_in++;
		vec[4*i]   = em_float<3,4>((d >> 7) & 0x1, (d >> 4) & 0x7, d & 0xf);
		vec[4*i+1] = em_float<3,4>((d >> 15) & 0x1, (d >> 12) & 0x7, (d >> 8) & 0xf);
		vec[4*i+2] = em_float<3,4>((d >> 23) & 0x1, (d >> 20) & 0x7, (d >> 16) & 0xf);
		vec[4*i+3] = em_float<3,4>((d >> 31) & 0x1, (d >> 28) & 0x7, (d >> 24) & 0xf);
	}

	 /* compute each row one by one, we can pipeline */
	for(int j = 0; j < m; j++){
		#pragma HLS pipeline II=1
		em_float<3,4> res = em_float<3,4>(0,0,0);
		for(int i = 0; i < 4; i++){
			d = *data_in++;
			res = res.add(vec[4*i].mult(em_float<3,4>((d >> 7) & 0x1, (d >> 4) & 0x7, d & 0xf)));
			res = res.add(vec[4*i+1].mult(em_float<3,4>((d >> 15) & 0x1, (d >> 12) & 0x7, (d >> 8) & 0xf)));
			res = res.add(vec[4*i+2].mult(em_float<3,4>((d >> 23) & 0x1, (d >> 20) & 0x7, (d >> 16) & 0xf)));
			res = res.add(vec[4*i+3].mult(em_float<3,4>((d >> 31) & 0x1, (d >> 28) & 0x7, (d >> 24) & 0xf)));
		}
		*data_out++ = res.getBinary();
	}
}

/*
 * Get 4 data (of 8 bits) and output a 32bits value
 */
void pack(volatile ap_uint<32>* data_in, volatile ap_uint<32>*  data_out){
	#pragma AP interface axis port=data_in
	#pragma AP interface axis port=data_out
	#pragma AP interface ap_ctrl_none port=return

	#pragma HLS pipeline II=1

	ap_uint<32> d = *data_in++;
	ap_uint<32> res = ((d && 0xff) << 24);
	d = *data_in++;
	res = res | ((d && 0xff) << 16);
	d = *data_in++;
	res = res | ((d && 0xff) << 8);
	d = *data_in++;
	res = res | (d && 0xff);
	*data_out++ = res;
}

/*
 * Matrix vector multiplication A.B = C (without the pipelined operation)
 * A : (M by 16) (with 4 float in 32 bits)
 * B : (16)		 (with 4 float in 32 bits)
 * C : (M)	 	 (with 1 float in 32 bits)
 */
void vector_matrix_mult_no(volatile ap_uint<32>* data_in, volatile ap_uint<32>*  data_out){
	#pragma AP interface axis port=data_in
	#pragma AP interface axis port=data_out
	#pragma AP interface ap_ctrl_none port=return

	ap_uint<32> m = *data_in++;
	em_float<3,4> vec[16];
	ap_uint<32> d;
	for(int i = 0; i < 4; i++){
		d = *data_in++;
		vec[4*i]   = em_float<3,4>((d >> 7) & 0x1, (d >> 4) & 0x7, d & 0xf);
		vec[4*i+1] = em_float<3,4>((d >> 15) & 0x1, (d >> 12) & 0x7, (d >> 8) & 0xf);
		vec[4*i+2] = em_float<3,4>((d >> 23) & 0x1, (d >> 20) & 0x7, (d >> 16) & 0xf);
		vec[4*i+3] = em_float<3,4>((d >> 31) & 0x1, (d >> 28) & 0x7, (d >> 24) & 0xf);
	}

	for(int j = 0; j < m; j++){
		em_float<3,4> res = em_float<3,4>(0,0,0);
		for(int i = 0; i < 4; i++){
			d = *data_in++;
			res = res.add(vec[4*i].mult(em_float<3,4>((d >> 7) & 0x1, (d >> 4) & 0x7, d & 0xf)));
			res = res.add(vec[4*i+1].mult(em_float<3,4>((d >> 15) & 0x1, (d >> 12) & 0x7, (d >> 8) & 0xf)));
			res = res.add(vec[4*i+2].mult(em_float<3,4>((d >> 23) & 0x1, (d >> 20) & 0x7, (d >> 16) & 0xf)));
			res = res.add(vec[4*i+3].mult(em_float<3,4>((d >> 31) & 0x1, (d >> 28) & 0x7, (d >> 24) & 0xf)));
		}
		*data_out++ = res.getBinary();
	}
}

/*
 * Split the matrix to send work to multiple IP.
 * Can be use full if xillybus send data faster than our IP can handle them.
 * But this is not the case on our application.
 */
void split_vector_mult(volatile ap_uint<32>* data_in,
		volatile ap_uint<32>*  data_out_1, volatile ap_uint<32>*  data_out_2){
	#pragma AP interface axis port=data_in
	#pragma AP interface axis port=data_out_1
	#pragma AP interface axis port=data_out_2
	#pragma AP interface ap_ctrl_none port=return
	#pragma HLS pipeline II=1

	/* Get matrix first dim and send it to each IP */
	ap_uint<32> m = (*data_in++);
	m = m/2;
	*data_out_1++ = m;
	*data_out_2++ = m;

	/* Send the vector to each IP */
	for(int i = 0; i < 4; i++){
		ap_uint<32> d = *data_in++;
		*data_out_1++ = d;
		*data_out_2++ = d;
	}

	/* Send one row over 2 for each IP */
	for(int i = 0; i < m/4; i++){
		for(int j = 0; j < 4; j++){
			*data_out_1++ = *data_in++;
		}
		for(int j = 0; j < 4; j++){
			*data_out_2++ = *data_in++;
		}
	}
}

/*
 * Merge the data from multiple IP before sending them back throught xillybus.
 */
void merge_vector_mult(volatile ap_uint<32>* data_in_1, volatile ap_uint<32>* data_in_2,
		volatile ap_uint<32>*  data_out){
	#pragma AP interface axis port=data_in_1
	#pragma AP interface axis port=data_in_2
	#pragma AP interface axis port=data_out
	#pragma AP interface ap_ctrl_none port=return
	#pragma HLS pipeline II=1

	*data_out++ = *data_in_1++;
	*data_out++ = *data_in_2++;
}



/*
 * This part is functions used to launch the algorithm on two matrices
 * A.B = C
 * A : (M by 16) (with 4 float in 32 bits)
 * B : (16 by N) (with 4 float in 32 bits) and N%4 = 0
 * C : (M by N)		 (with 4 float in 32 bits)
 *
 * The matrix B should be sent by column
 */
void split_four_calc(volatile ap_uint<32>* data_in,
		volatile ap_uint<32>*  data_out_1,
		volatile ap_uint<32>*  data_out_2,
		volatile ap_uint<32>*  data_out_3,
		volatile ap_uint<32>*  data_out_4){
#pragma AP interface axis port=data_in
#pragma AP interface axis port=data_out_1
#pragma AP interface axis port=data_out_2
#pragma AP interface axis port=data_out_3
#pragma AP interface axis port=data_out_4
#pragma AP interface ap_ctrl_none port=return

	ap_uint<32> m = *data_in++;
	*data_out_1++ = m;
	*data_out_2++ = m;
	*data_out_3++ = m;
	*data_out_4++ = m;

	for(int i = 0; i < 4; i++){*data_out_1++ = *data_in++;}
	for(int i = 0; i < 4; i++){*data_out_2++ = *data_in++;}
	for(int i = 0; i < 4; i++){*data_out_3++ = *data_in++;}
	for(int i = 0; i < 4; i++){*data_out_4++ = *data_in++;}

	for(int i = 0; i < m; i++){
		#pragma HLS pipeline II=1
		ap_uint<32> d = *data_in++;
		*data_out_1++ = d;
		*data_out_2++ = d;
		*data_out_3++ = d;
		*data_out_4++ = d;
	}
}

void merge_four_val(volatile ap_uint<32>* data_in_1,
		volatile ap_uint<32>*  data_in_2,
		volatile ap_uint<32>*  data_in_3,
		volatile ap_uint<32>*  data_in_4,
		volatile ap_uint<32>*  data_out){
#pragma AP interface axis port=data_out
#pragma AP interface axis port=data_in_1
#pragma AP interface axis port=data_in_2
#pragma AP interface axis port=data_in_3
#pragma AP interface axis port=data_in_4
#pragma AP interface ap_ctrl_none port=return
#pragma HLS pipeline II=1

	ap_uint<32> res = *data_in_1++;
	ap_uint<32> d = *data_in_2++;
	res = res << 8 | (d & 0xff);
	d = *data_in_3++;
	res = res << 8 | (d & 0xff);
	d = *data_in_4++;
	res = res << 8 | (d & 0xff);
	*data_out++ = res;
}
