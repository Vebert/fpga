#include "floatingpoint.hpp"

#ifndef _EM_FLOAT_CPP_
#define _EM_FLOAT_CPP_

template<int E, int M>
ap_uint<1> em_float<E, M>::getSign(){
	return sig;
}

template<int E, int M>
ap_uint<E> em_float<E, M>::getExponent() {
	return exp;
}

template<int E, int M>
ap_uint<M> em_float<E, M>::getMantise() {
	return man;
}

template<int E, int M>
ap_uint<M+1> em_float<E, M>::getRealMantise(){
	return (ap_uint<M+1>) (1 << M) | getMantise();
}

template<int E, int M>
ap_uint<E+M+1> em_float<E, M>::getBinary(){
	return 	(((ap_uint<E+M+1>) 	this->getSign()) 		<< (M+E)) +
			(((ap_uint<E+M>) 	this->getExponent()) 	<< 	M) +
								this->getMantise();
}

template<int E, int M>
em_float<E, M> em_float<E, M>::mult(em_float<E, M> b){

		// Nan and wrong
	if(this->isNan() || this->isWrong()){return *this;}
	if(b.isNan() || b.isWrong()){return b;}

		// 0 maybe return em_float<E,M>(0,0,0) is better ?
	if(this->isZero()){
		if(b.isInf()){return em_float<E,M>(0,(1<<E)-1,(1<<M)-1);}
		return *this;
	}
	if(b.isZero()){
		if(this->isInf()){return em_float<E,M>(0,(1<<E)-1,(1<<M)-1);}
		return b;
	}

	ap_uint<1> sign = this->getSign() ^ b.getSign();

		// inf
	if(this->isInf()){
		return em_float<E, M>(sign, this->getExponent(), 0);
	}else if(b.isInf()){
		return em_float<E, M>(sign, b.getExponent(), 0);
	}

		// n * m
	ap_uint<2*M + 2> man = this->getRealMantise() * b.getRealMantise();

	ap_uint<E+1> exp = this->getExponent() + b.getExponent() + (man >> (2*M + 1));

	if(exp <= this->getBias()){ //Go to 0
		return em_float(sign, 0, 0);
	}else if(exp - this->getBias() >= (1 << E) - 1){ //Go to inf
		return em_float(sign, (1<<E)-1, 0);
	}
	return em_float<E, M>(sign, exp - this->getBias(), (man >> (M + (man >> (2*M + 1)))));
}

template<int E, int M>
em_float<E, M> em_float<E, M>::add(em_float<E, M> b){
		//Nan
	if(this->isNan()){return *this;}
	if(b.isNan()){return b;}
		//Wrong
	if(this->isWrong()){return *this;}
	if(b.isWrong()){return b;}

		//zero
	if(this->isZero()){return b;}
	if(b.isZero()){return *this;}

	ap_uint<1> sign = this->getSign() ^ b.getSign();
		//inf
	if(this->isInf()){
		if(b.isInf() && sign != 0){return em_float<E,M>(0, (1<<E)-1, (1<<M)-1);}
		return *this;
	}
	if(b.isInf()){
		return b;
	}

	em_float<E, M> max;
	em_float<E, M> min;
	if(this->getExponent() >  b.getExponent() ||
	  (this->getExponent() == b.getExponent() && this->getMantise() > b.getMantise())){
		max = *this;
		min = b;
	}else{
		max = b;
		min = *this;
	}

	ap_uint<E> delta = max.getExponent() - min.getExponent();
		// same sign
	if(sign == 0){
		ap_uint<M+2> mant = ((ap_uint<M+2>)max.getRealMantise()) + (min.getRealMantise() >> delta);
		ap_uint<M> man = mant >> (mant >> (M+1));
		ap_uint<E> exp = max.getExponent() + (mant >> (M+1));
		if(exp >= (1 << E) - 1){ //inf
			return em_float<E,M>(max.getSign(), (1<<E)-1, 0);
		}
		return em_float<E,M>(this->getSign(), exp, man);
	}else{
		ap_uint<M+1> mant = ((ap_uint<M+1>)max.getRealMantise()) - (min.getRealMantise() >> delta);
		if(mant == 0){	//both are equal
			return em_float<E,M>(0, 0, 0);
		}

		for(int i = 0; i<M+1;i++){
			#pragma HLS UNROLL
			if(mant >> (M - i) != 0){
				ap_uint<E> exp = max.getExponent() - i;
				if(i >= max.getExponent()){
					return em_float<E,M>(0,0,0);
				}
				ap_uint<M> man = (mant << i);
				return em_float<E,M>(max.getSign(), exp, man);
			}
		}
		//impossible
		return em_float<E,M>(0, 0, 0);
	}
}
#endif
