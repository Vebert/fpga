#ifndef ZEDMODULE
#define ZEDMODULE

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#include <fcntl.h>
#include <unistd.h>
#include <algorithm>
#include <vector>
#include <thread>

#include "data.hpp"

class Module{
public:
    Module(){}

	/* open/close the files to communicate with xillybus */   
    void load();
    void unLoad();

	/* write all the input then read all the outputs */
    void run(std::vector<Data*> in, std::vector<Data*> out);
	
	/* write an output from xillybus to memory */
    void receiveData(uint32_t* data, uint32_t size);
	/* write an input from memory to xillibus */
    void sendData(uint32_t* data, uint32_t size);

protected:
    int fdr;
    int fdw;
};

void threadSend(Module* m, std::vector<Data*> din);
void threadRecv(Module* m, std::vector<Data*> dout);

#endif
