#ifndef ZEDUTIL
#define ZEDUTIL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/mman.h>

struct memory_map{
 off_t  offset;
 size_t size;
 off_t  page_base;
 off_t  page_offset;
 void*  ptr;
};

void initZedUtil();

    /* Reprogram the PL part of the zedBoard (Not stable). */
void switchTo(const char* bitfile);

    /* useless */
void switchFromBoot(const char* bitfileName);

    /* stop all xillybus modules */
void stopModules();

    /* relaunch all xillybus modules */
void relaunchModules();

    /* show the value of the controls bits */
void printControlStatus(unsigned char* c);

    /* print the given bits */
void printBits(unsigned char* c, int range);

    /* get the access to memory */
int getMemoryMap(struct memory_map* map, int file);

void endStart(unsigned char* c);
void waitDone(unsigned char* c);
void wait(unsigned char* c);

#endif
