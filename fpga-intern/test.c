#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/mman.h>
//#include <asm/cachectl.h>

union val{
    uint32_t b;
    float f;
};

void matMult(volatile float *vect, volatile float *mat, volatile float *ret){
    int i, j;
    for(i = 0; i < 8; i++ ){
	float sum = 0;
	for(j = 0; j < 4; j++){
	    sum += vect[i] * mat[ j + i * 8];
	}
     	ret[i] = sum;
    }
}


const int addr_a = 0x40;
const int addr_c = 0x48;

const int group_id_x = (int) 0x14;
const int group_id_y = (int) 0x18;
const int group_id_z = (int) 0x20;

const int global_offset_x = (int) 0x28;
const int global_offset_y = (int) 0x30;
const int global_offset_z = (int) 0x38;

int main(){
 initZedUtil();
 switchTo("/mnt/mmcblk0p1/pipelin.bit");
 int mem = open("/dev/mem", O_RDWR | O_SYNC);
 
 int num = 8;
 size_t pagesize = sysconf(_SC_PAGE_SIZE);
 size_t len = 0x60;
 int chunk = 4096;
 size_t svec = chunk*sizeof(int);
 
 off_t offset[] = {
 0x43c00000, 0x43c10000, 0x43c20000, 0x43c30000,
 0x43c40000, 0x43c50000, 0x43c60000, 0x43c70000
                   };

 off_t pagebase[num];// = {0,0,0,0};
 off_t page_offset[num];// = {0,0,0,0};
 off_t offset_a[num];// = {0,0,0,0};
 off_t offset_c[num];// = {0,0,0,0}; 
 off_t pagebase_a[num];// = {0,0,0,0};
 off_t pagebase_c[num];// = {0,0,0,0};
 off_t page_offset_a[num];// = {0,0,0,0};
 off_t page_offset_c[num];// = {0,0,0,0};

 volatile int* a[num];// = {0,0,0,0};
 volatile int* c[num];// = {0,0,0,0};
 unsigned char* ctrl[num];// = {0,0,0,0};

 int l;
 for(l=0; l<num; l++){
  pagebase[l] = (offset[l]/pagesize)*pagesize;
  page_offset[l] = offset[l] - pagebase[l];
 
  offset_a[l] = 0x04000000 + (0x01000000 * l);
  offset_c[l] = 0x05000000;// + (0x01000000 * l);
  pagebase_a[l] = (offset_a[l]/pagesize)*pagesize;
  pagebase_c[l] = (offset_c[l]/pagesize)*pagesize;
  page_offset_a[l] = offset_a[l] - pagebase_a[l];
  page_offset_c[l] = offset_c[l] - pagebase_c[l];
 
  a[l] = mmap(NULL, page_offset_a[l] + svec, PROT_READ | PROT_WRITE, MAP_SHARED, mem, pagebase_a[l]);
  c[l] = mmap(NULL, page_offset_c[l] + svec, PROT_READ | PROT_WRITE, MAP_SHARED, mem, pagebase_c[l]);
  ctrl[l] = mmap(NULL, page_offset[l] + len, PROT_READ | PROT_WRITE, MAP_SHARED, mem, pagebase[l]);
  printControlStatus(ctrl[l]); printf("\n\r");
  *(ctrl[l] + group_id_x) = 0;
  *(ctrl[l] + group_id_y) = 0;
  *(ctrl[l] + group_id_z) = 0;
  *(ctrl[l] + global_offset_x) = 0;
  *(ctrl[l] + global_offset_y) = 0;
  *(ctrl[l] + global_offset_z) = 0;
  
  *((off_t*) (ctrl[l] + addr_a)) = pagebase_a[l];
  *((off_t*) (ctrl[l] + addr_c)) = pagebase_c[l]; // + 2000; 
 }

 int i, inum;
 FILE* result = fopen("res.txt", "w");
 time_t timer;
 struct timeval start_time;
 struct timeval end_time;
 int size = (1 << 18);
 for(inum = 1; inum < num + 1; inum++){
  printf("start with %d \n\r", inum);
  for(size = (1 << 15); size < (1<<18); size = size << 1){
   time(&timer);
   gettimeofday(&start_time, NULL);
   for(i = 0; i < size; i++){
    for(l=0; l<inum; l++){
     *((off_t*) (ctrl[l] + addr_a)) = pagebase_a[l];
     *((off_t*) (ctrl[l] + addr_c)) = pagebase_c[l]; 
     (ctrl[l])[0] = (ctrl[l])[0] | (int) 0x01;
    }
    for(l=0; l<inum; l++){
     endStart(ctrl[l]);
     wait(ctrl[l]); 
    }
   }
   gettimeofday(&end_time, NULL);
   int second = difftime(time(NULL), timer);
   int micro = 1000000*(end_time.tv_sec - start_time.tv_sec) + end_time.tv_usec - start_time.tv_usec;
   printf("sec : %d ... micro %d \n\r", second, micro);
   fprintf( result, "%d, %f \n\r", inum, inum*((double) chunk)*size/second);
   printf("time : %d at size %d, rate %f bytes/sec \n\r", second, size, inum*((double) chunk)*size/second);
  }
 }
  
 for(l=0; l<num; l++){
  munmap((void*) a[l], svec);
  munmap((void*) c[l], svec);
  munmap((void*) ctrl[l], len);
 }
 fclose(result);
 close(mem);
 return 0;
}
