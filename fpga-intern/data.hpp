#ifndef ZEDDATA
#define ZEDDATA

#include <stdlib.h>
#include <stdint.h>

class Data{
public:
    Data(uint32_t id, uint32_t size, uint32_t pos){
	this->id = id;
	this->size = size;
	this->pos = pos;
	this->valid = false;
        this->data = NULL;}
    ~Data(){free(this->data);}

	/* Allocate the memory for this data if not already done. */
    void alloc();

	/* Allow to transmit data on the network. */
    void setUsable(){this->valid = true;}
 	/* Check if the datas are usable (the computation is finished). */
    bool canUse(){return this->valid;}

	/* Getters. */
    uint32_t* getData(){return this->data;}
    uint32_t getPos(){return this->pos;}
    uint32_t getId(){return this->id;}
    uint32_t getSize(){return this->size;}
private:
    uint32_t id;
    uint32_t size;
    uint32_t pos;

    bool valid; 

    uint32_t* data;
};

#endif
