#include "zedUtil.h"
#include <sys/time.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <thread>

void threadWrite(int fdw, int32_t* data_in, int size, int realloop, int loop, int MAX_SIZE);
void threadRead(int fdr, int32_t* data_out, int size, int realloop, int loop, int MAX_SIZE);
void funcReadWrite(int fdw, int fdr, int32_t* a, int32_t* b, int size, int realloop, int loop, int MAX_SIZE);

int main(){
 initZedUtil();
 switchTo("/mnt/mmcblk0p1/pipelin.bit");
 int fdr = open("/dev/xillybus_read_32", O_RDONLY);
 int fdw = open("/dev/xillybus_write_32", O_WRONLY);

 FILE* result = fopen("res.txt", "w");

 int MAX_SIZE = 2048*2048;
 int32_t* a = (int32_t*) malloc(MAX_SIZE*sizeof(int32_t));
 int32_t* b = (int32_t*) malloc(MAX_SIZE*sizeof(int32_t));

 int size, loop = 1, realLoop = 1;
 for(size = 2; size < MAX_SIZE; size = size*2){

   printf("start test at size %d \n\r", size);

   int done, l;
   time_t timer;
   time(&timer);

	//Bandwidth test.
   //std::thread writ (threadWrite, fdw, a, size, realLoop, loop, MAX_SIZE);
   //std::thread read (threadRead,  fdr, b, size, realLoop, loop, MAX_SIZE);
   //writ.join();
   //read.join();
   funcReadWrite(fdw, fdr, a, b, size, realLoop, loop, MAX_SIZE);

   int second = difftime(time(NULL), timer);
   if(second < 10){
        if(size > 1){
	 size = size/2;
	}
        realLoop = realLoop*2;
    	printf("too fast (%d), restart \n\r", second);
        continue;
   }

   fprintf(result, "%d, %f \n\r", size, ((double) realLoop)*loop*(2048*sizeof(int32_t)/second));
   printf("end in %d seconds. (at %f byte/sec) \n\r", second,((double) realLoop)*loop*(2048*sizeof(int32_t)/second));
   if(loop < 2048){
     loop = loop*2;
   }
   if(second > 40 && realLoop > 1){
     realLoop = realLoop/2;
   }
 }

 close(fdr);
 close(fdw);
 fclose(result);
}

void funcReadWrite(int fdw, int fdr, int32_t* a, int32_t* b, int size, int realloop, int loop, int MAX_SIZE){
 int l, done;
 for(l = 0; l < realloop; l++){
  for(done = 0; (done < MAX_SIZE) && (done < loop*2048); done += size){
   int retw = 0; int retr = 0;
   while(retw < size*sizeof(int32_t) && retr < size*sizeof(int32_t)){
     retw += write(fdw, &a[done], size*sizeof(int32_t) - retw);
     retr += read(fdr, &b[done], size*sizeof(int32_t) - retr);
   } 
  }
 }
}

void threadWrite(int fdw, int32_t* a, int size, int realloop, int loop, int MAX_SIZE){
 int l, done;
 //double sum = 0;
 for(l = 0; l < realloop; l++){
  for(done = 0; (done < MAX_SIZE) && (done < loop*2048); done += size){
   int ret = 0;
   while(ret < size*sizeof(int32_t)){
    int tmp = write(fdw, &a[done], size*sizeof(int32_t) - ret);
    //printf("write %d more %d  with total of %f               \r", tmp, ret, sum);
    if(tmp>0){ret+=tmp;}
   }
   //sum += ret;
  }
 }
 //printf("end write , %f                    \n\r", sum);
}

void threadRead(int fdr, int32_t* b, int size, int realloop, int loop, int MAX_SIZE){
 int l, done;
 //double sum = 0;
 for(l = 0; l < realloop; l++){
  for(done = 0; (done < MAX_SIZE) && (done < loop*2048); done += size){
   int ret = 0;
   while(ret < size*sizeof(int32_t)){
    int tmp = read(fdr, &b[done], size*sizeof(int32_t) - ret);
    //printf("read %d more %d with total of %f                  \r",tmp, ret, sum);
    if(tmp>0){ret+=tmp;}
   }
   //sum += ret;
  }
 }
 //printf("end read , %f                    \n\r", sum);
}
