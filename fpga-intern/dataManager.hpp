#ifndef ZEDDATAMANAGER
#define ZEDDATAMANAGER

#include <map>
#include <vector>
#include <stdexcept>
#include <mutex>

class DataManager;

#include "networkManager.hpp"
#include "data.hpp"

class DataManager{
public:
    DataManager(NetworkManager* nm){	this->nm = nm;
					this->readers = 0;}

	/* register a new data on this platform */
    Data* addData(uint32_t id, uint32_t size, uint32_t pos);

	/* get the value of a data on this platform */
    uint32_t* getValue(uint32_t id);

	/* get a data on this platform */
    Data* getData(uint32_t id);

	/* remove a data from this platform */
    void clear(uint32_t id);

    	/* get datas values from others platforms */
    void loadDatas(std::vector<Data*> toLoad);

	/* prevent concurent write */
    void readerIn();
    void readerOut();
    void writerIn();
    void writerOut();

private:
    NetworkManager* nm;
    std::map<uint32_t, Data*> datas;

    std::vector<std::pair<uint32_t, uint32_t>> pending;

    std::mutex mutEntry;
    std::mutex mutReader;
    int readers; 
};

#endif
