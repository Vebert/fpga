#include "task.hpp"

void Task::registerInput(DataManager* dm, uint32_t id, uint32_t size, uint32_t pId){
    this->din.push_back(dm->addData(id, size, pId));
}

void Task::registerOutput(DataManager* dm, uint32_t id, uint32_t size, uint32_t pId){
    this->dout.push_back(dm->addData(id, size, pId));
    this->dout.back()->alloc();
}

void Task::loadData(DataManager* dm){
    dm->loadDatas(this->din);
}

uint32_t Task::getTotalSize(){
    uint32_t t = 0;
    for(Data* d : this->din){	
	t += d->getSize();
    }
    return t;
}

void Task::launch(DataManager* dm, NetworkManager* nm){
    this->module->load();
    dm->writerIn();
    this->module->run(this->din, this->dout);
    dm->writerOut();
    this->module->unLoad();
    for(Data* d : this->dout){
    	d->setUsable();
    	nm->preventAvailable(d->getId());
    }
}
