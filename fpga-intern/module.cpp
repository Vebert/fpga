#include "module.hpp"

void Module::load(){
    fdr = open("/dev/xillybus_read_32", O_RDONLY);
    fdw = open("/dev/xillybus_write_32", O_WRONLY);

    if((fdr < 0) || (fdw < 0)){
	perror("Failed to open Xillybus");
        exit(1);
    }
}

void Module::unLoad(){
    close(fdr);
    close(fdw);
}

void Module::run(std::vector<Data*> din, std::vector<Data*> dout){
   std::thread send(threadSend, this, din);
   std::thread recv(threadRecv, this, dout);
   send.join();
   recv.join();
}

void threadSend(Module* m, std::vector<Data*> din){
    for(Data* d : din){
	m->sendData(d->getData(), d->getSize()*sizeof(uint32_t));	
    }
}

void threadRecv(Module* m, std::vector<Data*> dout){
    for(Data* d : dout){
	m->receiveData(d->getData(), d->getSize()*sizeof(uint32_t));
    }
}    

void Module::receiveData(uint32_t* data, uint32_t size){
    int pos = 0;
    while(pos < size){
	int tmp = read(this->fdr, data + pos/sizeof(uint32_t), size - pos);
        if(tmp>0){pos+=tmp;}
    }
}

void Module::sendData(uint32_t* data, uint32_t size){
    int pos = 0;
    while(pos < size){
        int tmp = write(this->fdw, data + pos/sizeof(uint32_t), size - pos);
        if(tmp < 0){
 	    printf("error %d \r", errno);
	}else{
	    pos+=tmp;
	}
    }
}
