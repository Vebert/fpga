#include "networkManager.hpp"

NetworkManager::NetworkManager(std::string server, uint32_t id, BitfileManager* bm){
    this->context = zmq_init(2); //Create the context give the number of thread used by zmq.
    this->server = server;
    this->selfId = id;
    this->bm = bm;

	//Open ports
    this->socket_da   = zmq_socket(this->context, ZMQ_REP);
    this->socket_pull = zmq_socket(this->context, ZMQ_PULL);
    this->socket_task = zmq_socket(this->context, ZMQ_PULL);
    this->socket_pub  = zmq_socket(this->context, ZMQ_PUB);

    bind(this->socket_da,   "tcp://eth0:65223");
    bind(this->socket_pull, "tcp://eth0:65224");
    bind(this->socket_task, "tcp://eth0:65225");
    
    zmq_connect(this->socket_pub, "tcp://192.168.1.1:65222");
	
   	 //Connect to server
    void* serv = zmq_socket(this->context, ZMQ_REQ);
	printf("connect to serveur \n\r");
    zmq_connect(serv, "tcp://192.168.1.1:65220");
	printf("send msg to serv \n\r");
    zmq_send(serv, &id, sizeof(uint32_t), 0); 
	printf("recv msg from serv \n\r");       
    zmq_recv(serv, &id, sizeof(uint32_t), 0);

	printf("close socket \n\r");
    zmq_close(serv);
}

Task NetworkManager::getNextTask(){
    zmq_msg_t msg;
    zmq_msg_init(&msg);

    zmq_recvmsg(this->socket_task, &msg, 0);

    size_t size = zmq_msg_size(&msg)/sizeof(uint32_t);
    uint32_t* data = (uint32_t*) zmq_msg_data(&msg);

    printf("msg size %d \n\r", size);
    printf("%x \n\r", data[0]);
    printf("%x \n\r", data[1]);

    Task task(data[0], this->bm->prepareModule(data[1]));
    bool input = true;
    for(int i = 0; i < (size-3)/3; i++){
	if(input && data[3*i+2] > 0){
   	    printf("new in %d %d %d \n\r", data[3*i+2], data[3*i+3], data[3*i+4]);
            task.registerInput(this->dm, data[3*i + 2], data[3*i + 3], data[3*i + 4]);
	}else{ // seperator
	    input = false;
            task.registerOutput(this->dm, data[3*i+3], data[3*i + 4], data[3*i + 5]);
        }
    }

    zmq_msg_close(&msg);
    return task;
}

NetworkManager::~NetworkManager(){
    zmq_close(this->socket_da);
    zmq_close(this->socket_pull);
    zmq_close(this->socket_task);

    zmq_term(this->context);
}

void bind(void* socket, std::string endPoint){
    if(socket == NULL){
	throw "NULL socket can't be binded.";
    }
    const char* c_endPoint = endPoint.c_str();
    int ret = zmq_bind(socket, c_endPoint);
    if(ret != 0){
	throw "Can't bind socket";
    }
}

void* NetworkManager::askData(uint32_t dataId, uint32_t serverId){
    void* socket_dr = zmq_socket(this->context, ZMQ_REQ);
    std::string addr = "tcp://192.168.1.";
    addr.append(std::to_string(serverId));
    addr.append(":65223");
    const char* caddr = addr.c_str();
    zmq_connect(socket_dr, caddr); 
    printf("ask Data %d to %s \n\r", dataId, caddr);

    zmq_send(socket_dr, &dataId, sizeof(uint32_t), 0);
    return socket_dr; 
}

int NetworkManager::storeData(uint32_t* ptr, uint32_t range, void* socket){
    int size = zmq_recv(socket,(void*) ptr, range*sizeof(int32_t), ZMQ_DONTWAIT);
    if(size >= 0){zmq_close(socket);}
    return size;
}

void NetworkManager::waitDataRequest(){
    uint32_t req;
    zmq_recv(this->socket_da, (void*) &req, sizeof(uint32_t), 0);
    dm->readerIn();
    Data* d = this->dm->getData(req);
    if(d != NULL && d->canUse()){
        zmq_send(this->socket_da, d->getData(), d->getSize()*sizeof(uint32_t), 0);
    }else{
        zmq_send(this->socket_da, NULL, 0, 0);	
    }
    dm->readerOut();
}

void NetworkManager::preventAvailable(uint32_t id){
    zmq_send(this->socket_pub, &id, sizeof(uint32_t), 0);
}

void NetworkManager::initSubscriber(){
    this->socket_sub = zmq_socket(this->context, ZMQ_SUB);
    zmq_connect(this->socket_sub, "tcp://192.168.1.1:65221");
    zmq_setsockopt(this->socket_sub, ZMQ_SUBSCRIBE, "", 0);
}

uint32_t NetworkManager::checkSubscriber(){
    uint32_t id = 0;
    int ret = zmq_recv(this->socket_sub, &id, sizeof(uint32_t), ZMQ_DONTWAIT);
    if(ret == -1){
	return 0;
    }
    return id;
}

void NetworkManager::closeSubscriber(){
    zmq_close(this->socket_sub);
}
