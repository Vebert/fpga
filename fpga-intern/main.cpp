#include "main.hpp"

uint32_t global_selfId;

void showData(DataManager* dm, int id){
    if(dm->getValue(id) == NULL){
	printf("data %d is not available \n\r", id);
    }else{
	printf("data %d %p : %d \n\r", id, dm->getValue(id), *dm->getValue(id));
    }
}

int main(int argc, char** argv){
    readConfig();
    BitfileManager bm;
    bm.addModule("/bitfiles/moduleIncr.txt");
    bm.addModule("/bitfiles/moduleDecr.txt");
    bm.addModule("/bitfiles/mats.txt");

    bm.addModule("/bitfiles/moduleMatVec.txt");
    bm.addModule("/bitfiles/moduleMatVec80.txt");
    bm.addModule("/bitfiles/moduleMatVecNo.txt");
    bm.addModule("/bitfiles/moduleMatVecSplit.txt");
    bm.addModule("/bitfiles/moduleMatMat4.txt");


    bm.addModule("/bitfiles/moduleLoop.txt");
    bm.addModule("/bitfiles/moduleKeepOnes.txt");

    NetworkManager nm("192.168.1.1", global_selfId, &bm);
    DataManager dm(&nm);
    nm.registerDM(&dm);
    initZedUtil();

    std::thread rT (requestReader, &nm, &dm);
    if(argc>1){
     std::thread tT (taskReader, &nm, &dm, argv[1]);
     tT.join();
    }else{
     std::thread tT (taskReader, &nm, &dm, "res.txt");
     tT.join();
    }
    rT.join();
    
    return 0; 
}

void taskReader(NetworkManager *nm, DataManager *dm, const char* file){ 
    FILE* res = fopen(file, "w");
    timeval start, start2;
    timeval end, end2;
    while(true){
        printf("Wait for next task. \n\r");
    	Task task = nm->getNextTask();
        if(task.getId() == 0){
	 break;
	}
        printf("Start loading data. \n\r");

		//Get the data
     	gettimeofday(&start, NULL);
        task.loadData(dm);
     	gettimeofday(&end, NULL);

        int millis = (end.tv_sec * 1000) + (end.tv_usec/1000) 
			- (start.tv_sec * 1000) - (start.tv_usec/1000);
        printf("Get data in %d ms.\n\r start running.\n\r", millis);

		//Run the module
     	gettimeofday(&start2, NULL);
        task.launch(dm, nm);
     	gettimeofday(&end2, NULL);

        int millis2 = (end2.tv_sec * 1000) + (end2.tv_usec/1000) 
			- (start2.tv_sec * 1000) - (start2.tv_usec/1000);
        printf("run in %d ms \n\r", millis2);
        fprintf(res, "%d, %d \n", millis, millis2);
    }
    printf("FPGA stop \n\r");
    fclose(res);
}

//warning STL only CR-safe
void requestReader(NetworkManager *nm, DataManager *dm){
  while(true){
    nm->waitDataRequest();  
  }
}

void readConfig(){
//    std::ifstream file("config");
//    file >> global_selfId;
//    file.close();
    FILE* fp = popen("ip addr show eth0 | grep 'inet ' | sed 's/ /\\n/g' | sed 's/\\./\\n/g' | grep / | sed 's/\\//a\\n/g' | grep a | sed 's/a//g'","r");
    if(fp == NULL){return;} //error
    
    char path[200];
    while (fgets(path, 200, fp) != NULL){
	global_selfId = atoi(path);
    }

    pclose(fp);
}

//do not work
int memoryMap(){
    off_t offset = 0x43C00000;
    size_t len = 0x64;

    size_t pageSize = sysconf(_SC_PAGE_SIZE);
    off_t page_base = (offset/pageSize)*pageSize;
    off_t page_offset = offset - page_base;

    int fd = open("/dev/mem", O_SYNC);
    void* mem =  mmap(NULL, page_offset + len, 
				PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, page_base);

    void* a = malloc(128*128*sizeof(uint32_t));
    void* b = malloc(128*128*sizeof(uint32_t));
    void* c = malloc(128*128*sizeof(uint32_t));

    if(mem == MAP_FAILED){
	perror("can't map memory");
	return -1;
    }

    free(a);
    free(b);
    free(c);

    return 0;
}
