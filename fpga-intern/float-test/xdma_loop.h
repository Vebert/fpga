#ifndef XDMA_LOOP_H
#define XDMA_LOOP_H

#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>

/* Hardware util */

#define ADDR_CTRL 	0x00
#define ADDR_GIE  	0x04
#define ADDR_IER	0x08
#define ADDR_ISR	0x0c
#define ADDR_DATA_IN	0x10
#define ADDR_DATA_OUT	0x18
#define SIZE_DATA_IN	32
#define SIZE_DATA_OUT	32

/* functions */

#define SUCCESS			0
#define DEVICE_NOT_FOUND	1
#define OPEN_DEVICE_FAILED	2
#define COMPONENT_IS_READY	3

typedef struct{
 uint32_t BaseAddress;
} XDma_loop;

void writeRegister(uint32_t BaseAddress, uint32_t RegOffset, uint32_t Data);
uint32_t readRegister(uint32_t BaseAddress, uint32_t RegOffset);

int initialize(XDma_loop* ptr, const char* name);
int release(XDma_loop* ptr);

void start(XDma_loop* ptr);
uint32_t isDone(XDma_loop* ptr);
uint32_t isReady(XDma_loop* ptr);
uint32_t isIdle(XDma_loop* ptr);

void setDataIn(XDma_loop* ptr, uint32_t data);
uint32_t getDataIn(XDma_loop* ptr);
void setDataOut(XDma_loop* ptr, uint32_t data);
uint32_t getDataOut(XDma_loop* ptr);

#endif
