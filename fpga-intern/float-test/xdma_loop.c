#include "xdma_loop.h"

void writeRegister(uint32_t BaseAddress, uint32_t RegOffset, uint32_t Data){
 *(volatile uint32_t*)((BaseAddress) + (RegOffset)) = Data;
}

uint32_t readRegister(uint32_t BaseAddress, uint32_t RegOffset){
 return *((uint32_t*)(BaseAddress + RegOffset));
}

void start(XDma_loop* ptr){
 uint32_t data;
 //assert !
 
 data = readRegister(ptr->BaseAddress, ADDR_CTRL) & 0x80;
 writeRegister(ptr->BaseAddress, ADDR_CTRL, 0x00);
}

uint32_t isDone(XDma_loop* ptr){
 uint32_t data;
 //assert
 
 data = readRegister(ptr->BaseAddress, ADDR_CTRL);
 return (data >> 1) & 0x1;
}

uint32_t isIdle(XDma_loop* ptr){
 uint32_t data;
	//assert
 data = readRegister(ptr->BaseAddress, ADDR_CTRL);
 return (data >> 2) & 0x1; 
}

uint32_t isReady(XDma_loop* ptr){
 uint32_t data;
 //assert
 data = readRegister(ptr->BaseAddress, ADDR_CTRL);
 return !(data & 0x1);
}

void setDataIn(XDma_loop* ptr, uint32_t data){
 writeRegister(ptr->BaseAddress, ADDR_DATA_IN, data);
}

uint32_t getDataIn(XDma_loop* ptr){
 return readRegister(ptr->BaseAddress, ADDR_DATA_IN);
}

void setDataOut(XDma_loop* ptr, uint32_t data){
 writeRegister(ptr->BaseAddress, ADDR_DATA_OUT, data);
}

uint32_t getDataOut(XDma_loop* ptr){
 return readRegister(ptr->BaseAddress, ADDR_DATA_OUT);
}
