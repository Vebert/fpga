#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/mman.h>
#include "../zedUtil.h"
#include "xdma_loop.h"

const int offset = 0x40400000;

//Control:
const int ap_start = 0x01;
const int ap_done  = 0x02;
const int ap_idle  = 0x04;
const int ap_ready = 0x08;

//Data signal:
const int data_in  = 0x10;
const int data_out = 0x18;

int main(){
  //Select the bitfile we need.
 initZedUtil();
 switchTo("/mnt/mmcblk0p1/pipelin.bit");

  //Get an access to the physical memory.
 int dev_mem = open("/dev/mem", O_RDWR | O_SYNC);
 struct memory_map map = {offset, 0x30, 0, 0, NULL};
 getMemoryMap(&map, dev_mem);

 printControlStatus((unsigned char*) map.ptr);printf("\n\r");

 struct memory_map map_in  = {0x04000000, 0x100, 0, 0, NULL};
 struct memory_map map_out = {0x05000000, 0x100, 0, 0, NULL};
 getMemoryMap(&map_in, dev_mem);
 getMemoryMap(&map_out, dev_mem);

 XDma_loop loop = {(uint32_t) map.ptr};

 setDataIn(&loop, (uint32_t) 0x04000000);
 setDataOut(&loop, (uint32_t) 0x05000000);
 printf("map in : %010x, map_out : %010x \n\r", readRegister(loop.BaseAddress, ADDR_DATA_IN), 
					  readRegister(loop.BaseAddress, ADDR_DATA_OUT));
 printf("map in : %010x, map_out : %010x \n\r", ((int*) map_in.ptr)[1], ((int*) map_out.ptr)[1]);
 start(&loop);
 printControlStatus((void*) loop.BaseAddress);printf("\n\r");

 printf("map in : %010x, map_out : %010x \n\r", readRegister(loop.BaseAddress, ADDR_DATA_IN), 
					  readRegister(loop.BaseAddress, ADDR_DATA_OUT));

 printf("map in : %010x, map_out : %010x \n\r", ((int*) map_in.ptr)[1], ((int*) map_out.ptr)[1]);

 sleep(2);
 while(!(isDone(&loop) && isReady(&loop)));

 printf("Done \n\r");
 printf("map in : %010x, map_out : %010x \n\r", readRegister(loop.BaseAddress, ADDR_DATA_IN), 
					  readRegister(loop.BaseAddress, ADDR_DATA_OUT));
 printf("map in : %010x, map_out : %010x \n\r", ((int*) map_in.ptr)[1], ((int*) map_out.ptr)[1]);
 
 /*
 *((int*) (map.ptr + data_in))  = 0x04000000;
 *((int*) (map.ptr + data_out)) = 0x05000000;
 printf("map in : %d, map_out : %d \n\r", ((int*) map_in.ptr)[0], ((int*) map_out.ptr)[0]);
 *((int*) map.ptr) = *((int*) map.ptr) | ap_start;  

 
 printControlStatus((unsigned char*) map.ptr);printf("\n\r");
 endStart(map.ptr);
 printControlStatus((unsigned char*) map.ptr);printf("\n\r");

 printf("map in : %d, map_out : %d \n\r", ((int*) map_in.ptr)[0], ((int*) map_out.ptr)[0]);
 */
 close(dev_mem); 
 return 0;
}
