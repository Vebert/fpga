#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/mman.h>
#include "../zedUtil.h"
#include <thread>


void sendf(int fdw, uint32_t m, uint32_t* vec_in, uint32_t* mat_in);
void recvf(int fdr, uint32_t m, uint32_t* vec_out);
void sendData(int fdw, uint32_t* data, uint32_t size);
void recvData(int fdr, uint32_t* data, uint32_t size);

int main(){
 //Select the bitfile we need.
 initZedUtil();
 switchTo("/mnt/mmcblk0p1/pipelin.bit");

 int fdr = open("/dev/xillybus_read_32",  O_RDONLY);
 int fdw = open("/dev/xillybus_write_32", O_WRONLY);

	//maybe impose m%4 == 0 (can increase the output rate).
 uint32_t m = 1;
 uint32_t* vec_in = (uint32_t*) malloc(4 * sizeof(uint32_t));
 uint32_t* vec_ou = (uint32_t*) malloc((4*m + 4) * sizeof(uint32_t));
 uint32_t* mat_in = (uint32_t*) malloc(4*m * sizeof(uint32_t));
 vec_in[0] = 0x30000030;
 vec_in[1] = 0x00003030;
 vec_in[2] = 0x00303030;
 vec_in[3] = 0x30303030;
 
 mat_in[0] = 0x30303030;
 mat_in[1] = 0x00003000;
 mat_in[2] = 0x00000000;
 mat_in[3] = 0x30000000;
 //int i;
 //for(i=0; i<4*m; i++){mat_in[i] = 0x30303030;}
 vec_ou[0]= 0x0;

 printf("start \n\r");

 std::thread send(sendf, fdw, m, vec_in, mat_in);
 std::thread recv(recvf, fdr, m, vec_ou);
 send.join();
 recv.join();

 printf("end : %08x \n\r", vec_ou[0]); 

 close(fdr);
 close(fdw);

 return 0;
}

void sendf(int fdw, uint32_t m, uint32_t* vec_in, uint32_t* mat_in){
 sendData(fdw, &m, sizeof(uint32_t));
 sendData(fdw, vec_in, 4*sizeof(uint32_t));
 sendData(fdw, mat_in, 4*m*sizeof(uint32_t));
}

void recvf(int fdr, uint32_t m, uint32_t* vec_out){
 recvData(fdr, vec_out, m*sizeof(uint32_t));
}

void sendData(int fdw, uint32_t* data, uint32_t size){
 int pos = 0;
 while(pos < size){
  int tmp = write(fdw, data + pos/sizeof(uint32_t), size - pos);
  if(tmp < 0){
   printf("error %d \r", errno);
  }else{
   pos += tmp;
  }
 }
}

void recvData(int fdr, uint32_t* data, uint32_t size){
 int pos = 0;
 while(pos < size){
  int tmp = read(fdr, data + pos/sizeof(uint32_t), size - pos);
  if(tmp>0){pos+=tmp;}
 }
}
