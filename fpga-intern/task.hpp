#ifndef ZEDTASK
#define ZEDTASK

#include <stdint.h>
#include <string>
#include <vector>
#include <ctime>

class Task;

#include "dataManager.hpp"
#include "module.hpp"
#include "bitfileManager.hpp"

/*
 * Task : sum up where are the data. use unique id.
 */
class Task{
public:
    Task(uint32_t id, Module* module){	this->id = id;
                                        this->end = false;
                                        this->loaded = 0;
                                        this->module = module; }

	/* provide info on where to found data */
    void registerInput(DataManager* dm, uint32_t id, uint32_t size, uint32_t pId);

    void registerOutput(DataManager* dm, uint32_t id, uint32_t size, uint32_t pId);

        /* provide info on which node need data */
    void registerChildren(uint32_t id, std::string pos);

	/* allocate memory and ask for data to others */
    void loadData(DataManager* nm);

	/* run module */
    void launch(DataManager* dm, NetworkManager* nm);

	/* prevent from ending send data if someone asked them */
    void finish();

        /* prevent if data are availables */
    bool isEnded(){return end;}

    void setOuput(uint32_t id, uint32_t size);

    uint32_t getTotalSize();

    uint32_t getId(){return this->id;}

private:
    bool end;

    uint32_t loaded;
    uint32_t id;

    std::vector<Data*> din;
    std::vector<Data*> dout;

    Module* module;
};
#endif
