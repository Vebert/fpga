#include <zmq.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <thread>
#include <sys/time.h>

#include "zedUtil.h"
#include "bitfileManager.hpp"
#include "networkManager.hpp"
#include "dataManager.hpp"

int main(int argc, char** argv);

    /* read the config files and found the ip of the fpga. */
void readConfig();

    /* show the first uint32_t of a given data. */
void showData(DataManager* dm, int id);

    /* main thread of the fpga, read and execute the tasks. */
void taskReader(NetworkManager *nm, DataManager *dm, const char* file);

//void taskReader_save(NetworkManager *nm, DataManager *dm, char* file);

    /* second thread of the fpga, reply to data requests */
void requestReader(NetworkManager *nm, DataManager *dm);

   /* Do not work : try to access physical memory */
int memoryMap();
