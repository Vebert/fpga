#include "zedUtil.h" 

void initZedUtil(){
    system("mount /dev/mmcblk0p1 /mnt/mmcblk0p1");
}

/*
 * Max lenght of the bitfile name : 980
 */
void switchTo(const char* path){
    // Stop all xillyBus modules. 
    stopModules();

    // Rebuild the PL. 
    char cmd0[1000] = "cat "; // lenght 4
    char cmd1[20] = " > /dev/xdevcfg"; //lenght 15 
    strcat(cmd0, path);
    strcat(cmd0, cmd1); 
    system(cmd0);

    // Relaunch Xillybus modules
    relaunchModules();
}

void switchFromBoot(const char* bitfileName){
    char path[100] = "/mnt/mmcblk0p1/";
    strcat(path, bitfileName);
    switchTo(path);
}

void stopModules(){
    system("rmmod -f xillybus_lite_of");
    system("rmmod -f xillybus_of");
    system("rmmod -f xillybus_core");
}

void relaunchModules(){
    system("modprobe xillybus_core");
    system("modprobe xillybus_of");
    system("modprobe xillybus_lite_of");
}

/* axi bus */
void printControlStatus(unsigned char* c){
 printBits(c, 8);
}

void printBits(unsigned char* c, int range){
 int l;
 for(l = 0; l < range; l++){
   printf("%d", 0x01 & (c[l/16] >> (l%16)));
 }
}

void printHexa(unsigned char* c, int range){
 int l;
 for(l = 0; l < range; l++){
  printf("%02x.", c[l]);
 }
}

void endStart(unsigned char* c){
 while((c[0] & 0x01) != 0){};
}

void waitDone(unsigned char* c){
 while((c[0] & 0x02) == 0);
}

void wait(unsigned char* c){
 while( (c[0] & 0xe) == 0 ){};
}
void waitValue(unsigned char* c, unsigned char mask){
 while(c[0] & mask == 0){}
}

 //TODO error detection
int getMemoryMap(struct memory_map* map, int file){
 size_t pagesize = sysconf(_SC_PAGE_SIZE);
 map->page_base = (map->offset/pagesize)*pagesize;
 map->page_offset = map->offset - map->page_base;
 map->ptr = mmap(NULL, 
		 map->page_offset + map->size, 
		 PROT_READ | PROT_WRITE, 
		 MAP_SHARED, 
		 file, 
		 map->page_base);
 return 0;
}
