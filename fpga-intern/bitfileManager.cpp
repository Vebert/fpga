#ifndef ZEDBITFILEMANAGERIMPL
#define ZEDBITFILEMANAGERIMPL

#include "bitfileManager.hpp"

Module* BitfileManager::prepareModule(uint32_t id){
    printf("ask for module %d \n\r", id);
    const char* cpath = this->bitfiles.at(id).c_str();

    switchTo(cpath);

    return this->modules.at(id);
}

void BitfileManager::addModule(std::string modulePath){
    std::ifstream file(modulePath);
    uint32_t id;
    std::string bitfilePath;

    file >> id;
    file >> bitfilePath;
    printf("register module %d \n\r", id);
    this->bitfiles[id] = bitfilePath;
    this->modules[id] = new Module();

    file.close();
}
#endif
