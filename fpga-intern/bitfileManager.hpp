#ifndef ZEDBITFILEMANAGER
#define ZEDBITFILEMANAGER

#include "zedUtil.h"
#include <string>
#include <map>
#include "module.hpp"
#include <fstream>

class BitfileManager{
public:
    BitfileManager(){};

    Module* prepareModule(uint32_t id);

    void addModule(std::string modulePath);
private:
    std::map<uint32_t, std::string> bitfiles;
    std::map<uint32_t, Module*> modules;
};

#endif
