#include "dataManager.hpp"

Data* DataManager::addData(uint32_t id, uint32_t size, uint32_t pos){
    try{
	Data* d = this->datas.at(id);
        if(size != d->getSize()){
	    throw 10;
	}
	return d;
    }catch(const std::out_of_range& e){	
    	this->datas[id] = new Data(id, size, pos);
    	return this->datas.at(id);
    }
}

uint32_t* DataManager::getValue(uint32_t id){
    try{
	Data* data = this->datas.at(id);
	return data->getData();
    }catch(const std::out_of_range& e){	
    	return NULL;
    }
}

Data* DataManager::getData(uint32_t id){
    try{
	return this->datas.at(id);
    }catch(const std::out_of_range& e){
	return NULL;
    }   
}

void DataManager::clear(uint32_t id){
    this->datas.erase(id);
}

void DataManager::loadDatas(std::vector<Data*> toLoad){
    nm->initSubscriber();
    int loaded = 0;
    std::vector<void*> sockets;
    for(Data* d : toLoad){
        if(d->canUse()){
	    loaded++;
	}else{
	sockets.push_back(this->nm->askData(d->getId(), d->getPos()));
	}
    }
    int i = 0;
    printf("already load %d / %d \n\r", loaded, toLoad.size());
    std::vector<Data*> waitSub;
    while(loaded < toLoad.size() + waitSub.size()){
	Data* d = toLoad[i];
        if(!d->canUse()){
            d->alloc();
       	    int ret = nm->storeData(d->getData(), d->getSize(), sockets[i]);
       	    if(ret > 0){
                printf("receive size %d. \n\r", ret);
            	d->setUsable();
         	loaded++;
	    }else if(ret == 0){
		printf("receive empty message. \n\r");
		waitSub.push_back(d);
		toLoad.erase(toLoad.begin() + i);
		sockets.erase(sockets.begin() + i);
 	    }
	}
        uint32_t subResult = nm->checkSubscriber();
	int j;
	for( j = 0; j < waitSub.size(); j++){
            Data* d = waitSub[j];
	    if(subResult == d->getId()){
		sockets.push_back(this->nm->askData(d->getId(), d->getPos()));
	    	toLoad.push_back(d);
		waitSub.erase(waitSub.begin() + j);
		break;
	    }
       	}
	i = (i+1)%toLoad.size();        
    }
    nm->closeSubscriber();
}

void DataManager::readerIn(){
    this->mutEntry.lock();
    this->mutReader.lock();
    this->readers++;
    this->mutReader.unlock();
    this->mutEntry.unlock();
}

void DataManager::readerOut(){
    this->mutReader.lock();
    this->readers--;
    this->mutReader.unlock();
}

void DataManager::writerIn(){
    this->mutEntry.lock();
    while(this->readers > 0);
}

void DataManager::writerOut(){
    this->mutEntry.unlock();
}
