#ifndef ZEDNETWORKMANAGER
#define ZEDNETWORKMANAGER

#include <zmq.h>
#include <string>
#include <cstring>
#include <stdio.h>

class NetworkManager;

#include "task.hpp"
#include "bitfileManager.hpp"

void bind(void* socket, std::string endPoint);

class NetworkManager{
public:
    NetworkManager(std::string server, uint32_t id, BitfileManager* bm);
   ~NetworkManager();

    int connect();
 
	/* Pull a task from master */
    Task getNextTask();

	/* Send req for distant data */
    void* askData(uint32_t taskId, uint32_t serverId);

	/* Store rep data directly on memory */
    int storeData(uint32_t* ptr, uint32_t range, void* socket);

	/* Wait other machine request for local data */
    void waitDataRequest();

	/* Publish new data available on the platform */
    void preventAvailable(uint32_t dataId);

	/* Register subscriber socket */
    void initSubscriber();

	/* get the next message on the subscriber socket */
    uint32_t checkSubscriber();

	/* Close subscriber socket*/
    void closeSubscriber();

    void registerDM(DataManager* dm){this->dm = dm;}

private:
    BitfileManager* bm;
    DataManager* dm;

    uint32_t selfId;
    void* context;

	//thread 1
    void* socket_da;	//Data ack socket type : ZMQ_REP.
    void* socket_ds;	//Data sender socker type : ZMQ_PUSH

	//thread 0
    void* socket_pull; 	//Receive data. type ZMQ_PULL
    void* socket_task;  //Ask new task from the server. type ZMQ_PULL
    void* socket_pub;   //Prevent if a new data is available on this server.
    void* socket_sub;   //Check if new data arrive.

    std::string server;
};

#endif
