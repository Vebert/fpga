import server
import fpga
import time
import random
import numpy as np
import numpy.random as rd
import float_util as fu

def matrix_vector_mult(serv, matrix, vector, i, fpga_id=2, id_task=1):
 serv.registerData(4*i + 1, 1, 1, fpga.pack([len(matrix)/4]))
 serv.registerData(4*i + 2, 4, 1, fpga.pack(vector))
 serv.registerData(4*i + 3, len(matrix), 1, fpga.pack(matrix))
 serv.registerData(4*i + 4, len(matrix)/4, fpga_id, "")

 serv.attribTask(fpga_id, id_task, 4, [4*i+1, 4*i+2, 4*i+3], [4*i+4])
 return 4*i+4

def loop(serv, data, i, fpga_id=2, id_task = 1):
 serv.registerData(4*i+1, len(data), 1, fpga.pack(data))
 serv.registerData(4*i+2, len(data), fpga_id, "")

 serv.attribTask(fpga_id, id_task, 5, [4*i+1], [4*i+2])

def keepOnes(serv, data, i, fpga_id=2, id_task=1):
 serv.registerData(4*i+1, len(data), 1, fpga.pack(data))
 serv.registerData(4*i+2, len(data) - np.count_nonzero([x-1 for x in data]), fpga_id, "")

 serv.attribTask(fpga_id, id_task, 8, [4*i+1], [4*i+2])

def ex_keep():
 serv = server.server()
 serv.launch()
 time.sleep(1)

 i = 2
 for j in range(5):
  m = 4096*pow(2,i)

  data = [0]*(m-16) + 16*[1]
  keepOnes(serv, data, j)
 serv.attribTask(2,0,8,[],[])

def ex_matVec():
  #create a new server
 serv = server.server()
  #launch the server
 serv.launch()
 time.sleep(1) #sleep so fpga had time to connect.

 i = 11
 for j in range(5):
  #create data
  m = 4096 * pow(2,i) 
  vector = rd.randint(0, (1<<32), 4, dtype=np.uint32)
  matrix = rd.randint(0, (1<<32), m, dtype=np.uint32)
  matrix_vector_mult(serv, matrix, vector, j, 2) #register data and attrib the task
 serv.attribTask(2,0,4,[],[])

def check_data():
 serv = server.server()
 serv.launch()
 time.sleep(1)

 matrix = []
 for x in range(256):
  matrix = matrix + [0,0,0,x]

 val_matrix = [fu.getValue_8(x) for x in matrix]
 for v in range(256):
  vector = [0, 0, 0, v]
  res_id = matrix_vector_mult(serv, matrix, vector, v, 2)

  data = serv.data(res_id)
  val_data = [x for x in fpga.unpack(data._value)]
  print(str(len(matrix)) + " " + str(len(val_data)))
  
  for i in range(len(val_data)):
   fu.check_mult(v, matrix[4*i+3], val_data[i], 111, 239)
  print("pass " + str(v))

def ex_loop():
 serv = server.server()

 serv.launch()
 time.sleep(1)

 i = 11
 for j in range(10):
  m = 4096*pow(2,i)
  data = [0]*m
  loop(serv, data, j, 2)
 serv.attribTask(2,0,5,[],[])
