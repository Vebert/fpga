import zmq
import struct
import data

 # convert an array of in to an bytes string.
def pack(L):
 Pack = struct.pack("<L", L[0])
 for e in L[1:]:
  Pack = Pack + struct.pack("<L", e)
 return Pack
  
def unpack(B):
 return [struct.unpack("<L", B[i: i+4])[0] for i in range(0, len(B), 4)]
 
class fpga:
 def __init__(self, i, serv):
  self._id = i
  self._serv = serv
  addr = "tcp://192.168.1.%d:65225" %i
  self._tSocket = self._serv._ctx.socket(zmq.PUSH)
  self._tSocket.connect(addr)

  # send a new task to the fpga
 def sendTask(self, id, module, dins, douts):
  Pack = pack([id, module])
  for d in dins:
   Pack = Pack + pack(d.getInfo())
  Pack = Pack + pack([0])
  for d in douts:
   Pack = Pack + pack(d.getInfo())
   d._pos = self._id
  self._tSocket.send(Pack)
