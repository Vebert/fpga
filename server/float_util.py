import math

def getValue_8(v):
 n = v%128
 s = 1 if n==v else -1
 if n == 0:
  return 0.0
 if n < 16 or n > 112:
  return float("Nan")
 if n == 112:
  return s*float("Inf")
 m = n%16
 e = (n-m)/16
 f = 1.0
 for i in range(4):
  f = f + ((m >> i) & 1)*pow(2, i - 4)
 if(e>=3):
  return s*f*pow(2, e-3)
 else:
  return s*f/pow(2, 3-e)

def check_mult(a, b, c, maxi, mini):
 na = getValue_8(a)
 nb = getValue_8(b)
 nc = getValue_8(c)
 maxi = getValue_8(maxi)
 mini = getValue_8(mini)

 if(na*nb == nc or (math.isnan(na*nb) and math.isnan(nc))):
  return 0
 if(math.isinf(nc) and 
	((na*nb>maxi and nc>0) or (na*nb<mini and nc<0))):
  return 0
 else:
  if abs(nc - na*nb) < 0.4:
   return abs(nc - (na*nb))
  print("err : " + str(na) + " * " + str(nb) + " = "
		 + str(na*nb) + " vs " + str(nc))
  return abs(nc - (na*nb))

