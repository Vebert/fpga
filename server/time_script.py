import server
import time
import fpga

a = server.server();  
a.launch();

time.sleep(2)

print("start ... ")

for i in range(20, 30):
  print("launch on size " + str(pow(2, i)))
  start = time.time()
  a.registerData(i, pow(2,i), 1, fpga.pack([1]*pow(2,i)))
  a.registerData(i+11, pow(2,i), 2, "")
  reg = time.time()
  a.attribTask(2, i, 1, [i], [i+11])
  d = a.data(i+11)
  end = time.time()
  print("end in : " + str(end - start) + " register took : " + str(reg-start))

time.sleep(10)
