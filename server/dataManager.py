import zmq
import data

class dataManager:

  def __init__(self, server):
   self._server = server
   self._datas = {}
    
    # register a new data
  def registerData(self, id, size, pos, val):
   self._datas[id] = data.data(id, size, pos)
   self._datas[id].setValue(val)

    # convert a array of is to an array of data
  def getDatas(self, ids):
   return [self._datas[e] for e in ids]

   # get one data (load it if not on the server)
  def getData(self, id):
   d = self._datas[id]
   if(d._pos == 1):
    return d
   else:
    self.getDataBack([d])
    return self.getData(id)

   # ask to an fpga to send one of the data.
  def getDataBack(self, toLoads):
   self._server.initSub()
   loaded = 0
   sockets = []
   for d in toLoads:
    if(d._pos != 1):
     sockets.append(self._server.askData(d))
    else:
     loaded+=1

   i = 0
   waitSub = []
   while(loaded < len(toLoads) + len(waitSub)):
    if(len(toLoads) > 0):
     d = toLoads[i]
     if(d._pos != 1):
      ret = self._server.storeData(d, sockets[i])
      if(ret > 0):          
       loaded+=1
      elif(ret == 0):
       waitSub.append(d)
       toLoads.pop(i)
       sockets.pop(i)
   
    subResult = self._server.checkSub()
    for j in range(len(waitSub)):
     d = waitSub[j]
     if(subResult == d._id):
      sockets.append(self._server.askData(d))
      toLoads.append(d)
      waitSub.pop(j)
      break

   self._server.closeSub()
