import struct

class data:
 def __init__(self, id, size, pos):
  self._id = id
  self._size = size
  self._pos = pos
  self._value = ""

  # get the info that define a data.
 def getInfo(self):
  return [self._id, self._size, self._pos]

  # set the value of a data (should be a binary) (type : bytes)
 def setValue(self, v):
  self._value = v
