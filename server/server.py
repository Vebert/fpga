import zmq
import struct
import threading
import data
import fpga
import dataManager

class server:

 def __init__(self):
  self._ctx = zmq.Context()
	#socket to handle first connection ("handcheck")
  self._cSocket = self._ctx.socket(zmq.REP)
  self._cSocket.bind("tcp://*:65220")

	#socket to handle data request
  self._dSocket = self._ctx.socket(zmq.REP)
  self._dSocket.bind("tcp://*:65223")

	# sockets to handle xpub/xsub mechanism for the network
	# it prevent others when a data become available
  self._xPub = self._ctx.socket(zmq.PUB)
  self._xSub = self._ctx.socket(zmq.SUB)
  self._xPub.bind("tcp://*:65221")
  self._xSub.bind("tcp://*:65222")
  self._xSub.setsockopt(zmq.SUBSCRIBE, "")

	# dict of the connected fpga
  self._fpgas = {}
	# data manager
  self._dm = dataManager.dataManager(self)

  # register a new connexion       
 def addFPGA(self, recv):
  I = struct.unpack("<L",recv)[0]
  self._fpgas[I] = fpga.fpga(I, self)

  # wait for new connexions
 def listenConnexion(self):
  while(True):
   Recv = self._cSocket.recv()
   if(len(Recv) == 4):
    print("connect new fpga.")
    self.addFPGA(Recv)
    self._cSocket.send(Recv)
   else:
    print("receive unvalid connexion request.")

  # wait for data request (if a data is not here we will load it)
 def listenAskData(self):
  while(True):
   Recv = self._dSocket.recv()
   print("receive of len %d" % len(Recv))
   if(len(Recv) == 4):
    DataId = struct.unpack("<L", Recv)[0]
    self._dSocket.send(self._dm.getData(DataId)._value)
    print("Data %d sent" % DataId)

  # ask data for a remote fpga
 def askData(self, Data):
  Sock = self._ctx.socket(zmq.REQ)
  Sock.connect("tcp://192.168.1.%d:65223" % Data._pos)
  Mess = struct.pack("<L", Data._id)
  Sock.send(Mess)
  return Sock
  
  # xSub/xPub thread
 def listenAndProp(self):
  zmq.device(zmq.FORWARDER, self._xSub, self._xPub) 

  # add new data.
 def registerData(self, id, size, pos, val):
  self._dm.registerData(id, size, pos, val)

  # send a task to a remote fpga
 def attribTask(self, fpga, id, module, dins, douts):
  dataIn = self._dm.getDatas(dins)
  dataOut = self._dm.getDatas(douts)
  self._fpgas[fpga].sendTask(id, module, dataIn, dataOut)

  # receive new value for a data
 def storeData(self, data, socket):
  try:
   Recv = socket.recv(zmq.NOBLOCK)
   if(len(Recv) > 0):
    data.setValue(Recv)
    data._pos = 1
    socket.close()
   return len(Recv)   
  except Exception as e:
   return -1

  # return a data (load it if not here).
 def data(self, id):
  return self._dm.getData(id)

  # Sub are used to get data from other fpga.
 def initSub(self):
  self._socketSub = self._ctx.socket(zmq.SUB)
  self._socketSub.connect("tcp://192.168.1.1:65221")
  self._socketSub.setsockopt(zmq.SUBSCRIBE, "")

 def checkSub(self):
  try:
   Recv = self._socketSub.recv(zmq.NOBLOCK)
   return struct.unpack("<L", Recv)[0]
  except Exception as e:
   return 0
   
 def closeSub(self):
  self._socketSub.close()

 def launch(self):
  Tlap = threading.Thread(target=server.listenAndProp, args=(self,))
  Tlad = threading.Thread(target=server.listenAskData, args=(self,))
  Tlc  = threading.Thread(target=server.listenConnexion, args=(self,))
  Tlap.daemon = True;
  Tlad.daemon = True;
  Tlc.daemon = True;
  Tlap.start()
  Tlad.start()
  Tlc.start()
  
  # create and launch a test server
def cTS():
 a = server()
 a.registerData(1, 128*128, 1, fpga.pack([1]*128*128))
 a.registerData(2, 128*128, 1, fpga.pack([1]*128*128))
 a.registerData(3, 128*128, 1, fpga.pack([2]*128*128))
 a.registerData(4, 128*128, 2, "")
 a.registerData(5, 128*128, 3, "")

 Tlap = threading.Thread(target=server.listenAndProp, args=(a,))
 Tlad = threading.Thread(target=server.listenAskData, args=(a,))
 Tlc  = threading.Thread(target=server.listenConnexion, args=(a,))
 Tlap.daemon = True;
 Tlad.daemon = True;
 Tlc.daemon = True;

 Tlap.start()
 Tlad.start()
 Tlc.start()
 return a
