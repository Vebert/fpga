# README #

### What is this repository for? ###

This repository regroup the work on FPGA programmation we have done during summer 2017.

A wiki about this project (only the first step on FPGA) is available at [https://bitbucket.org/Vebert/fpga/wiki/Home](https://bitbucket.org/Vebert/fpga/wiki/Home) 

## Content
### fpga-intern
Code that run on the FPGA side, some files are just test code.
Most of the files are related to the client/server communication part, Makefile will compile this program. (It is not stable)

This program will need to load some files (see main.cpp) to link bitfiles to and unique id. Those files need to be on this format:
	id
	path_of_the_bitfile

the program will save the data transfert time throught network and the computation time in "res.txt", you can specify a other filename as an argument.

### hls
Contain the c++ code (with vivado HLS stuff) that was used to create the IP blocks.

### material
Contain the raw data from running programs and the jupyter code to plot the data.

### opencl
Contain the OpenCL kernel running on CPU/GPU machine and code in python using pyOpenCL to run it.

### server
Contain the python code used to communicate between a server machine some FPGA.