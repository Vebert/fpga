#define TYPE uchar 

#define S_MAN 4
#define S_EXP 3

#define M_MAN ((0x1 << S_MAN) - 1)
#define M_SIG (0x1 << S_MAN + S_EXP) 
#define M_EXP (M_SIG - 1) - M_MAN
#define BIAS ((0x1 << (S_MAN + S_EXP-1)) - 1 - M_MAN)

/*
 * TYPE should be unsigned and with value in N+ !
 * Only work with |mantise| <= 15 ! 
 */

bool isInf(TYPE a){
 return ((a & M_EXP) == M_EXP) && ((a & M_MAN) == 0);
}

bool isNan(TYPE a){
 return ((a & M_EXP) == M_EXP) && ((a & M_MAN) != 0);
}

bool isZero(TYPE a){
 return ((a & M_EXP) == 0) && ((a & M_MAN) == 0);
}

bool isWrong(TYPE a){
 return ((a & M_EXP) == 0) && ((a & M_MAN) != 0);
}

TYPE getRealMantise(TYPE a){
 return (((TYPE) M_MAN) + 1) | (a & M_MAN);
}

TYPE mult_float(TYPE a, TYPE b){
 if(isNan(a)){return a;}
 if(isNan(b)){return b;}

 if(isWrong(a)){return a;}
 if(isWrong(b)){return b;}
 if(isZero(a)){
  if(isInf(b)){return M_EXP | M_MAN;}   
  return a;}
 if(isZero(b)){
  if(isInf(a)){return M_EXP | M_MAN;}   
  return b;}

 TYPE sign = (a & M_SIG) ^ (b & M_SIG);
 
 if(isInf(a)){return sign | M_EXP;}
 if(isInf(b)){return sign | M_EXP;}

 unsigned int mant = getRealMantise(a)*getRealMantise(b);
 TYPE exp = (a & M_EXP) + (b & M_EXP) + ((mant >> (2*S_MAN+1)) << S_MAN);

 //check 0
 if(exp <= BIAS){
  return sign;
 } 
 //check inf
 if(exp-BIAS >= M_EXP){
  return sign | M_EXP | 0;
 }
 return sign | (exp - BIAS) | ((mant >> (S_MAN + (mant >> (2*S_MAN+1)))) & M_MAN);
}

TYPE add_float(TYPE a, TYPE b){
 if(isNan(a)){return a;}
 if(isNan(b)){return b;}
 if(isWrong(a)){return a;}
 if(isWrong(b)){return b;}

 if(isZero(a)){return b;}
 if(isZero(b)){return a;}

 if(isInf(a)){
  TYPE sign = (a & M_SIG) ^ (b & M_SIG);
  if(isInf(b) && sign != 0){return M_EXP | M_MAN;}
  return a;
 }
 if(isInf(b)){
  return b;
 }

 TYPE max;
 TYPE min;
 if((a & M_EXP) >  (b & M_EXP) || 
   ((a & M_EXP) == (b & M_EXP) && (a & M_MAN) > (b & M_MAN))){
  max = a;
  min = b;
 }else{
  max = b;
  min = a;
 }

 TYPE delta = ((max & M_EXP) - (min & M_EXP)) >> S_MAN;
 if((max & M_SIG) == (min & M_SIG)){
  TYPE mant = getRealMantise(max) + (getRealMantise(min) >> delta);
  TYPE man = (mant >> (mant >> (S_MAN + 1))) & M_MAN;
  TYPE exp = (max & M_EXP) + ((mant >> (S_MAN + 1)) << S_MAN);
  if(exp >= M_EXP){ //go to +inf
   return (max & M_SIG) | M_EXP | 0;
  }
  return (max & M_SIG) | exp | man;
 }else{ 
  TYPE mant = getRealMantise(max) - (getRealMantise(min) >> delta);
  if(mant == 0){
   return 0;
  }
  int i;
  for(i = 0; i < S_MAN + 1; i++){
   if((mant >> (S_MAN - i)) != 0){
    TYPE exp = (max & M_EXP) - (i << S_MAN);
    if(exp <= M_MAN || (max & M_EXP) < (i << S_MAN)){
     return 0;
    }
    TYPE man = (mant << i) & M_MAN;
    return (max & M_SIG) | exp | man;
   }
  }
  return 0; //impossible
 }
}

kernel void mat_mult(global const int *a,
		     global const int *b,
                     global const int *k,
		     global int *c){
 size_t i = get_global_id(0);
 size_t j = get_global_id(1);
 size_t Sj = get_global_size(1);

 int tmp = 0;
 int l;
 for(l=0;l<k[0];l++){
  tmp = add_float(tmp, mult_float(a[i*k[0] + l], b[l*Sj + j]));
 }
 c[i*Sj+j] = tmp;
}

/*
kernel void mult(global const int *a,
		 global const int *b,
                 global int *c){
 size_t i = get_global_id(0);
 size_t j = get_global_id(1);
 size_t Sj = get_global_size(1);

 c[i*Sj + j] = (add_float(a[i] & 0xFF, b[j] & 0xFF)); 
 //c[i*Sj + j] = (mult_float(a[i] & 0xFF, b[j] & 0xFF)); 
}
*/
