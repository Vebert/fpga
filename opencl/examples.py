import pyopencl as cl
import pyopencl.array as cl_array
import raw_code as rc
import tool
import numpy
import time
import math

def get_double_precision_gpu():
	return get_double_precision(cl.device_type.GPU)

def get_double_precision(dtype = cl.device_type.ALL):
	return tool.get_devices_where_one(["cl_khr_fp64", "cl_amd_fp64"], dtype)

def get_gpu():
	return tool.get_devices_where_all([], cl.device_type.GPU)

def get_cpu():
	return tool.get_devices_where_all([], cl.device_type.CPU)


def get_one_context(dic):
	for key in dic.keys():
		if len(dic[key]) != 0:
			return cl.Context(dic[key]), dic[key]
	raise NameError('No compute unit availables.')	

def Mat_mult_gpu(a = numpy.random.rand(5000*5000).astype(numpy.float32),
		 b = numpy.random.rand(5000*5000).astype(numpy.float32), shape = [5000, 5000]):
	ctx, devices = get_one_context(get_gpu())
	kernel = rc.cl_mult
	return Mat_mult(ctx, devices[0], kernel, a, b, shape)

def Mat_mult_gpu_double(a = numpy.random.rand(5000*5000).astype(numpy.double),
			b = numpy.random.rand(5000*5000).astype(numpy.double), shape = [5000, 5000]):
	ctx, devices = get_one_context(get_double_precision_gpu())
	kernel = rc.cl_type_double + rc.cl_mult
	return Mat_mult(ctx, devices[0], kernel, a, b, shape)

def Mat_mult_double(	a = numpy.random.rand(5000*5000).astype(numpy.double),
			b = numpy.random.rand(5000*5000).astype(numpy.double), shape = [5000, 5000]):
	ctx, devices = get_one_context(get_double_precision())
	kernel = rc.cl_type_double + rc.cl_mult
	return Mat_mult(ctx, devices[0], kernel, a, b, shape)

def Vec_add_gpu(	a = numpy.random.rand(5000*5000).astype(numpy.float32),
			b = numpy.random.rand(5000*5000).astype(numpy.float32), shape = [5000, 5000]):
	ctx, devices = get_one_context(get_gpu())
	kernel = rc.cl_add
	return Vec_add(ctx, devices[0], kernel, a, b)

def Vec_add_gpu_double(	a = numpy.random.rand(5000*5000).astype(numpy.float32),
			b = numpy.random.rand(5000*5000).astype(numpy.float32), shape = [5000, 5000]):
	ctx, devices = get_one_context(get_double_precision_gpu())
	kernel = rc.cl_type_double + rc.cl_add
	return Vec_add(ctx, devices[0], kernel, a, b)

def Vec_add_double( 	a = numpy.random.rand(5000*5000).astype(numpy.float32),
			b = numpy.random.rand(5000*5000).astype(numpy.float32), shape = [5000, 5000]):
	ctx, devices = get_one_context(get_double_precision())
	kernel = rc.cl_type_double + rc.cl_add
	return Vec_add(ctx, devices[0], kernel, a, b)
	
	None
	
def Vec_add(ctx, device, kernel, a, b):
		#create the queue
	q = cl.CommandQueue(ctx, device)

		#send data to the device.
	a_dev, b_dev = tool.send_arrays_to_queue(a, (a, b))
	c_dev = cl_array.empty_like(a_dev)

		#Build the kernel
	prg = cl.Program(ctx, kernel).build()
	
		#Launch the computatiom
	t = time.time()
	prg.add(q, a.shape, None, a_dev.data, b_dev.data, c_dev.data)

        return a_dev, b_dev, c_dev, t

def Mat_mult(ctx, device, kernel, a, b, shape):

		#create the queue
	q = cl.CommandQueue(ctx, device)

		#send data to the device.
	a_dev, b_dev, s_1, s_2 = tool.send_arrays_to_queue(q, (a,b, numpy.array(shape[0], dtype=int),
			    					    numpy.array(shape[1], dtype=int)))
	c_dev = cl_array.zeros(q, (shape[0]*shape[0]), a_dev.dtype)

		#Build the kernel
	prg = cl.Program(ctx, kernel).build()
	
		#Launch the computation
	t = time.time()
	prg.mult(q, [shape[0]*shape[0]], None, a_dev.data, b_dev.data, c_dev.data, s_1.data, s_2.data)
	return a_dev, b_dev, c_dev, t

def float_test_l(kernel, a, b, k, dev=get_cpu()):
	ctx, devices = get_one_context(dev)
	q = cl.CommandQueue(ctx, devices[0])

        t = time.time() #time with communication and kernel build
	a_dev, b_dev, k_dev = tool.send_arrays_to_queue(q, (a, b, k))
	c_dev = cl_array.zeros(q, (a.shape[1], b.shape[0]), a_dev.dtype)
	
   	prg = cl.Program(ctx, kernel).build()
	prg.mat_mult(q, c_dev.shape, None, a_dev.data, b_dev.data, k_dev.data, c_dev.data)
	return a_dev, b_dev, c_dev, t

def float_test_s(kernel, a, b, k, dev=get_cpu()):
	ctx, devices = get_one_context(dev)
	q = cl.CommandQueue(ctx, devices[0])
 
	a_dev, b_dev, k_dev = tool.send_arrays_to_queue(q, (a, b, k))
	c_dev = cl_array.zeros(q, (a.shape[1], b.shape[0]), a_dev.dtype)

	prg = cl.Program(ctx, kernel).build()

	t = time.time() #time for computation only
	prg.mat_mult(q, c_dev.shape, None, a_dev.data, b_dev.data, k_dev.data, c_dev.data)
	return a_dev, b_dev, c_dev, t

def getValue(v):
 n = v%128
 s =  1 if n==v else -1
 if n == 0:
  return 0.0
 if n < 16 or n > 112:
  return float("NaN")
 if n == 112:
  return s*float("Inf")
 m = n%16
 e = (n - m)/16
 f = 1.0
 for i in range(4):
  f = f + ((m >> i) & 1)*pow(2, i - 4)
 if(e>=3):
  return s * f * pow(2, e - 3)
 else:
  return s * f / pow(2, 3 - e)

def check_add(a, b, c, maxi, mini): 
 na = getValue(a)
 nb = getValue(b)
 nc = getValue(c)
 maxi =  getValue(maxi)
 mini = getValue(mini)

 if(nb+na == nc):
  return 0
 elif(math.isnan(na+nb) and math.isnan(nc)):
  return 0
 elif(na+nb > maxi and math.isinf(nc) and nc > 0):
  return 0
 elif(na+nb < mini and math.isinf(nc) and nc < 0):
  return 0 
 else:
  if(math.isinf(nc - (na+nb)) or True):
   print("err : " + str(na) + " + " + str(nb) + " = "  + str(na+nb) + " vs " + str(nc))
   print("err : " + str(a) + " + " + str(b) + " = " + str(c))
  return abs(nc - (na+nb))

def check_mult(a, b, c, maxi, mini):
 na = getValue(a)
 nb = getValue(b)
 nc = getValue(c)
 maxi = getValue(maxi)
 mini = getValue(mini)

 if(na*nb == nc or (math.isnan(na*nb) and math.isnan(nc))):
  return 0
 if(math.isinf(nc) and ((na*nb>maxi and nc>0) or (na*nb<mini and nc<0))):
  return 0
 else:
  print("err : " + str(na) + " + " + str(nb) + " = "  + str(na*nb) + " vs " + str(nc))
  print("err : " + str(a) + " + " + str(b) + " = " + str(c))
  return abs(nc - (na*nb))
