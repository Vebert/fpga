#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pyopencl as cl
import raw_code as rc
import tool

import pyopencl.array as cl_array
import numpy as np
import numpy.linalg as la
import os
import time
import thread
import examples
import random
import numpy.random as rd

SA = 5000
SB = 5000

#uncomment this line to debug opencl compilation
#os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'

#run a matrix multiplication on a gpu
"""
print(examples.Mat_mult_gpu(
	np.random.rand(3000*2000).astype(np.float32), 
	np.random.rand(2000*3000).astype(np.float32), 
	[3000,2000]))
	#run a matrix multiplication on a core with double precision
print(examples.Mat_mult_double());
	#will crash because there is no gpu with double precision on that machine
print(examples.Mat_mult_gpu_double());
"""

#run 8-bit floating point test.
 #generate the random values
def getArrays(m):
 a = rd.randint(0, 255, 16*m, dtype=np.uint32).reshape((16,m))
 b = rd.randint(0, 255, 16, dtype=np.uint32).reshape((1,16))
 k = np.array([16], dtype=np.uint32)
 return a, b, k

def runTest(kernel, f, func, devices, size, repeat=10):
 print("start run")
 for i in range(size):
  print("i = %d" % i)
  m = 4096*pow(2,i)
  ltime = []
  for j in range(repeat): 
   a,b,k = getArrays(m)
   a,b,c,t = func(kernel,a,b,k,devices)
   ltime.append((time.time() - t)*1000)
  f.write("%d, %f \n" % (m, np.mean(ltime)))

  #read the kernel
kernel = tool.getOpenClFile("float.c")

 #cpu s
with open("res_cpus.txt", 'w') as f:
 runTest(kernel, f, examples.float_test_s, examples.get_cpu(), 13)

 #cpu l
with open("res_cpul.txt", 'w') as f:
 runTest(kernel, f, examples.float_test_l, examples.get_cpu(), 13)

 #gpu s
with open("res_gpus.txt", 'w') as f:
 runTest(kernel, f, examples.float_test_s, examples.get_cpu(), 10)

 #gpu l
with open("res_gpul.txt", 'w') as f:
 runTest(kernel, f, examples.float_test_l, examples.get_cpu(), 10)


 #Check valid results
"""
a = np.array(range(255), dtype=np.uint32).reshape((1,255))
b = np.array(range(255), dtype=np.uint32).reshape((255,1))
k = np.array([1], dtype=np.uint32)
a, b, c, t = examples.float_test(kernel, a, b, k)
np.set_printoptions(formatter={'int':hex})
c = c.get()
dt = time.time() - t

a = a.get()
b = b.get()
err = 0
print(c)
D = []
for i in range(255):
 for j in range(255):
  d = examples.check_mult(a[0][i], b[j][0], c[i][j], 111, 239)
  if(d != 0):
   D.append(d)  

D = np.array(D)
print("mean : " + str(np.mean(D)))
print("max : " + str(np.max(D)))
print(len(D))
"""
